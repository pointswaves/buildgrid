# Copyright (C) 2020 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# pylint: disable=redefined-outer-name

from datetime import datetime
import mock
import pytest

from buildgrid.server.metrics_utils import timed_method, ExceptionCounter, Counter
from buildgrid.server.metrics_utils import create_timer_record, create_counter_record
from tests.utils.metrics import mock_create_timer_record, mock_create_counter_record
from buildgrid._enums import MetricRecordDomain


class NoMonitoringBusObject:
    """ This class doesn't have a self._monitoring_bus """
    def __init__(self):
        pass

    @timed_method("test", MetricRecordDomain.UNKNOWN)
    def test(self):
        pass

    @ExceptionCounter("test", exceptions=[RuntimeError])
    def test_exception(self, should_raise):
        if should_raise:
            raise RuntimeError
        else:
            pass


def test_no_monitoring_bus_object():
    """ The lack of a self._monitoring_bus field throws an AttributeError """
    obj = NoMonitoringBusObject()
    with pytest.raises(AttributeError):
        obj.test()


class NoInstanceNameObject:
    """ This class has a self._monitoring_bus but no self._instance_name """
    def __init__(self):
        self._monitoring_bus = mock.Mock()

    @timed_method("test", MetricRecordDomain.UNKNOWN)
    def test(self):
        pass

    @timed_method("test", MetricRecordDomain.UNKNOWN, instanced=True)
    def test_instanced(self):
        pass

    @ExceptionCounter("test", exceptions=[RuntimeError, AttributeError])
    def test_exception(self, should_raise):
        if should_raise:
            raise RuntimeError

    @ExceptionCounter("test", exceptions=[RuntimeError, AttributeError])
    def test_different_exception(self):
        """ Raise different exceptions than expecting for metrics."""
        raise AssertionError

    @timed_method("test", MetricRecordDomain.UNKNOWN)
    @ExceptionCounter("test", exceptions=[RuntimeError])
    def test_multiple_decorators(self, should_raise):
        if should_raise:
            raise RuntimeError
        else:
            pass


def test_no_instance_object():
    """ The lack of a self._instance_name field throws an AttributeError
    when passing instanced=True in the decorator. """
    obj = NoInstanceNameObject()
    obj.test()
    obj._monitoring_bus.send_record_nowait.assert_called_once()
    with pytest.raises(AttributeError):
        obj.test_instanced()


class UnsetInstanceNameObject:
    """ This class has a self.instance_name but it's not set. """
    def __init__(self):
        self._monitoring_bus = mock.Mock()
        self._instance_name = None

    @timed_method("test", MetricRecordDomain.UNKNOWN)
    def test(self):
        pass

    @timed_method("test", MetricRecordDomain.UNKNOWN, instanced=True)
    def test_instanced(self):
        pass


def test_unset_instance_name_object():
    """ The non-instanced timed method works normally, but an assertion in
    the decorator catches the unset self._instance_name when instance=True is
    passed in the decorator. """
    obj = UnsetInstanceNameObject()
    obj.test()
    obj._monitoring_bus.send_record_nowait.assert_called_once()
    with pytest.raises(AssertionError):
        obj.test_instanced()


class NormalObject:
    """ This class has both self._monitoring_bus and self._instance_name. """
    def __init__(self):
        self._monitoring_bus = mock.Mock()
        self._instance_name = "foo"

    @timed_method("test", MetricRecordDomain.UNKNOWN)
    def test_return_5(self):
        return 5

    @timed_method("test", MetricRecordDomain.UNKNOWN, instanced=True)
    def test_instanced_return_6(self):
        return 6

    @timed_method("test", MetricRecordDomain.UNKNOWN)
    def test_raises_exception(self):
        raise ValueError

    @timed_method("test", MetricRecordDomain.UNKNOWN, instanced=True)
    def test_instanced_raises_exception(self):
        raise ValueError


def check_record_sent_and_reset(monitoring_bus):
    monitoring_bus.send_record_nowait.assert_called_once()
    monitoring_bus.reset_mock()


def test_normal_object():
    """ For a properly specified object, the methods that run without
    throwing an error publish metrics. The methods that throw an error
    do not. """
    obj = NormalObject()
    assert obj.test_return_5() == 5
    check_record_sent_and_reset(obj._monitoring_bus)
    assert obj.test_instanced_return_6() == 6
    check_record_sent_and_reset(obj._monitoring_bus)
    with pytest.raises(ValueError):
        obj.test_raises_exception()
    with pytest.raises(ValueError):
        obj.test_instanced_raises_exception()
    obj._monitoring_bus.send_record_nowait.assert_not_called()


@mock.patch("buildgrid.server.metrics_utils.datetime")
def test_decorator_raises_exception(mock_datetime):
    """ Make sure that if the decorator raises an exceptions, the value
    is still returned. """
    mock_datetime.now = mock.Mock()
    mock_datetime.now.side_effect = ValueError()
    obj = NormalObject()
    assert obj.test_return_5() == 5
    assert obj.test_instanced_return_6() == 6


def test_simple_counter():
    """ Validate counter in context manager """
    mock_monitor = mock.Mock()

    with Counter("test", mock_monitor) as c:
        c.increment()
        assert c.count == 1.0
        c.increment()
        assert c.count == 2.0
        c.count = 55.1
        assert c.count == 55.1

    mock_monitor.send_record_nowait.assert_called_once()


def test_simple_exception_counter():
    """ Validate exceptions counter in context manager """
    mock_monitor = mock.Mock()

    with pytest.raises(RuntimeError):
        with ExceptionCounter("test", exceptions=[RuntimeError], monitoring_bus=mock_monitor) as ec:
            assert ec.count == 1.0
            assert ec.instance_name is None
            raise RuntimeError

    mock_monitor.send_record_nowait.assert_called_once()


def test_no_publish_exception_counter():
    """ Validate exceptions counter doesn't publish. """
    mock_monitor = mock.Mock()

    with ExceptionCounter("test", exceptions=[RuntimeError], monitoring_bus=mock_monitor) as ec:
        assert ec.count == 1.0

    mock_monitor.send_record_nowait.assert_not_called()


def test_no_publish_different_exception_counter():
    """ Validate exceptions counter doesn't publish for different exceptions. """
    obj = NoInstanceNameObject()

    with pytest.raises(AssertionError):
        obj.test_different_exception()

    obj._monitoring_bus.send_record_nowait.assert_not_called()


def test_exception_decorator_counter():
    """ Validate exceptions counter decorator. """
    obj = NoInstanceNameObject()
    with pytest.raises(RuntimeError):
        obj.test_exception(should_raise=True)
    obj._monitoring_bus.send_record_nowait.assert_called_once()


def test_no_throw_decorator_counter():
    """ Validate exceptions counter decorator. """
    obj = NoInstanceNameObject()
    obj.test_exception(should_raise=False)
    obj._monitoring_bus.send_record_nowait.assert_not_called()


def test_decorator_errors():
    """ Test decorator errors """
    # No monitoring_bus but raises, shouldn't publish
    # or raise another excpetion like AttributeError
    obj = NoMonitoringBusObject()
    with pytest.raises(RuntimeError):
        obj.test_exception(should_raise=True)


def test_counter_no_monitoring_bus():
    """ Test no exceptions is raised when counter publishes without monitoring bus. """
    with Counter("test") as c:
            pass


def test_counter_exception_publish():
    """ Test not publishing when exceptions thrown in context manager"""

    mock_monitor = mock.Mock()

    with pytest.raises(RuntimeError):
        with Counter("test", mock_monitor) as c:
            c.increment(76.0)
            assert c.count == 76.0
            raise RuntimeError

    mock_monitor.send_record_nowait.assert_not_called()


@mock.patch('buildgrid.server.metrics_utils.create_timer_record', new=mock_create_timer_record)
@mock.patch('buildgrid.server.metrics_utils.create_counter_record', new=mock_create_counter_record)
def test_multiple_decorators():
    """ Test monitor called with multiple decorators. """
    obj = NoInstanceNameObject()
    obj.test_multiple_decorators(should_raise=False)
    obj._monitoring_bus.send_record_nowait.assert_called_once()

    timer_record = mock_create_timer_record(MetricRecordDomain.UNKNOWN, "test")

    call_list = [mock.call(timer_record)]
    obj._monitoring_bus.send_record_nowait.assert_has_calls(call_list)

    obj._monitoring_bus.reset_mock()
    with pytest.raises(RuntimeError):
        obj.test_multiple_decorators(should_raise=True)
    obj._monitoring_bus.send_record_nowait.assert_called_once()

    counter_record = mock_create_counter_record(MetricRecordDomain.UNKNOWN, "test")

    call_list = [mock.call(counter_record)]
    obj._monitoring_bus.send_record_nowait.assert_has_calls(call_list)
