# Copyright (C) 2020 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""
Cleanup command
=================

Create a BCS cleanup daemon
"""

import sys
import click

from buildgrid.cleanup.cleanup import CASCleanUp
from buildgrid.server.cas.storage.storage_abc import StorageABC
from buildgrid.server.cas.storage.index.index_abc import IndexABC
from buildgrid.server._monitoring import MonitoringOutputType, MonitoringOutputFormat

from ..cli import pass_context, setup_logging
from ..settings import parser


@click.group(name='cleanup', short_help="Start a local cleanup service.")
@pass_context
def cli(context):
    pass


@cli.command('start', short_help="Setup a new cleanup instance.")
@click.argument('CONFIG',
                type=click.Path(file_okay=True, dir_okay=False, exists=True, writable=False))
@click.option('-v', '--verbose', count=True,
              help='Increase log verbosity level.')
@click.option('--dry-run', is_flag=True,
              help='Do not actually cleanup CAS')
@click.option('--sleep-interval', type=int, help='Seconds to sleep inbetween calls to cleanup')
@click.option('--high-watermark', type=int, help='Storage size needed to trigger cleanup')
@click.option('--low-watermark', type=int, help='Storage size needed to stop cleanup')
@pass_context
def start(context, config, verbose, dry_run, high_watermark, low_watermark, sleep_interval):
    """Entry point for the bgd-server CLI command group."""
    setup_logging(verbosity=verbose)

    with open(config) as f:
        settings = parser.get_parser().safe_load(f)

    try:
        cleanup = _create_cleanup_from_config(settings, dry_run, high_watermark,
                                              low_watermark, sleep_interval)
        cleanup.start()

    except KeyError as e:
        click.echo("ERROR: Could not parse config: {}.\n".format(str(e)), err=True)
        sys.exit(-1)

    except KeyboardInterrupt:
        pass

    finally:
        cleanup.stop()


def _create_cleanup_from_config(configuration, dry_run, high_watermark, low_watermark, sleep_interval):
    """Parses configuration and setup a fresh server instance."""
    kargs = {'dry_run': dry_run, 'high_watermark': high_watermark,
             'low_watermark': low_watermark, 'sleep_interval': sleep_interval}

    try:
        instances = configuration['instances']

    except KeyError as e:
        click.echo("Error: Section missing from configuration: {}.".format(e), err=True)
        sys.exit(-1)

    try:
        instance_storages = {}
        instance_indexes = {}
        for instance in instances:
            instance_name = instance['name']
            storage_list = instance['storages']
            tmp_store = None
            tmp_index = None
            for storage in storage_list:
                if isinstance(storage, IndexABC):
                    tmp_index = storage
                elif isinstance(storage, StorageABC):
                    tmp_store = storage

            if tmp_store and tmp_index:
                instance_storages[instance_name] = tmp_store
                instance_indexes[instance_name] = tmp_index
            else:
                click.echo("Warning: Skipping instance {}.".format(instance_name), err=False)

        kargs['storages'] = instance_storages
        kargs['indexes'] = instance_indexes

    except KeyError as e:
        click.echo("Error: Storage/Index missing from configuration: {}.".format(e), err=True)
        sys.exit(-1)

    if 'monitoring' in configuration:
        monitoring = configuration['monitoring']

        try:
            if 'enabled' in monitoring:
                kargs['monitor'] = monitoring['enabled']

            if 'endpoint-type' in monitoring:
                kargs['mon_endpoint_type'] = MonitoringOutputType(monitoring['endpoint-type'])

            if 'endpoint-location' in monitoring:
                kargs['mon_endpoint_location'] = monitoring['endpoint-location']

            if 'serialization-format' in monitoring:
                kargs['mon_serialisation_format'] = MonitoringOutputFormat(monitoring['serialization-format'])

            if 'metric-prefix' in monitoring:
                # Ensure there's only one period at the end of the prefix
                kargs['mon_metric_prefix'] = monitoring['metric-prefix'].strip().rstrip('.') + "."

        except (ValueError, OSError) as e:
            click.echo("Error: Configuration, {}.".format(e), err=True)
            sys.exit(-1)

    return CASCleanUp(**kargs)
