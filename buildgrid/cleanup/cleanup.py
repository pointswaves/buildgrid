# Copyright (C) 2020 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import asyncio
import logging
import signal

from buildgrid.server.cas.storage.s3 import S3Storage
from buildgrid.server.cas.storage.index.sql import SQLIndex
from buildgrid.server._monitoring import MonitoringBus, MonitoringOutputType, MonitoringOutputFormat


class CASCleanUp:
    """Creates a LRU CAS cleanup service."""

    def __init__(self, dry_run, high_watermark, low_watermark, sleep_interval, storages,
                 indexes, monitor, mon_endpoint_type=None, mon_endpoint_location=None,
                 mon_serialisation_format=None, mon_metric_prefix=None):

        self._logger = logging.getLogger(__name__)

        self._dry_run = dry_run

        self._high_watermark = high_watermark
        self._low_watermark = low_watermark

        self._storages = storages
        self._indexes = indexes

        self._is_instrumented = monitor

        self._sleep_interval = sleep_interval

        self._main_loop = asyncio.get_event_loop()

        if self._is_instrumented:
            self._monitoring_bus = MonitoringBus(
                self._main_loop, endpoint_type=mon_endpoint_type,
                endpoint_location=mon_endpoint_location,
                metric_prefix=mon_metric_prefix,
                serialisation_format=mon_serialisation_format)

    # --- Public API ---

    def start(self, *, on_server_start_cb=None):
        """ Start cleanup service """
        self._main_loop.add_signal_handler(signal.SIGTERM, self.stop)
        if self._is_instrumented:
            self._monitoring_bus.start()

        for instance_name in self._storages:
            if self._storages[instance_name].is_cleanup_enabled():
                asyncio.ensure_future(self._cleanupWorker(instance_name))
            else:
                self._logger.info("CleanUp for instance {} skipped.".format(instance_name))
        self._main_loop.run_forever()

    def stop(self):
        """ Stops the cleanup service """
        if self._is_instrumented:
            self._monitoring_bus.stop()
        self._main_loop.stop()

    # --- Private API ---

    async def _cleanupWorker(self, instance_name):
        """ Cleanup when full """
        continue_cleanup = False
        storage = self._storages[instance_name]
        index = self._indexes[instance_name]
        while True:
            self._logger.info("CleanUp for instance {} started.".format(instance_name))
            await asyncio.sleep(self._sleep_interval)
            total_size = index.get_total_size()
            if total_size >= self._high_watermark:
                continue_cleanup = True
            elif total_size <= self._low_watermark:
                continue_cleanup = False
            if continue_cleanup:
                self._logger.info("Deleting items from storage/index for instance {}.".format(instance_name))
                pass
