# Copyright (C) 2018 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""
S3Storage
==================

A storage provider that stores data in an Amazon S3 bucket.
"""

import io
import logging

import boto3
from botocore.exceptions import ClientError

from .storage_abc import StorageABC


class S3Storage(StorageABC):

    def __init__(self, bucket, page_size=1000, **kwargs):
        self.__logger = logging.getLogger(__name__)

        self._bucket_template = bucket
        self._page_size = page_size
        self._s3 = boto3.resource('s3', **kwargs)

    def _get_bucket_name(self, digest):
        try:
            return self._bucket_template.format(digest=digest)
        except IndexError as e:
            self.__logger.error("Could not calculate bucket name for digest=[{digest}]. This"
                                " is either a misconfiguration in the BuildGrid S3 bucket "
                                "configuration, or a badly formed request.".format(digest=digest))
            raise

    def _construct_key(self, digest):
        return digest.hash + '_' + str(digest.size_bytes)

    def _multi_delete_blobs(self, bucket_name, digests):
        bucket = self._s3.Bucket(bucket_name)
        response = bucket.delete_objects(Delete={'Objects': digests})
        return_failed = []
        failed_deletions = response.get('Errors', [])
        for failed_key in failed_deletions:
            return_failed.append(failed_key['Key'])
        return return_failed

    def has_blob(self, digest):
        self.__logger.debug("Checking for blob: [{}]".format(digest))
        try:
            self._s3.Object(self._get_bucket_name(digest.hash),
                            self._construct_key(digest)).load()
        except ClientError as e:
            if e.response['Error']['Code'] not in ['404', 'NoSuchKey']:
                raise
            return False
        return True

    def get_blob(self, digest):
        self.__logger.debug("Getting blob: [{}]".format(digest))
        try:
            obj = self._s3.Object(self._get_bucket_name(digest.hash),
                                  self._construct_key(digest))
            return io.BytesIO(obj.get()['Body'].read())
        except ClientError as e:
            if e.response['Error']['Code'] not in ['404', 'NoSuchKey']:
                raise
            return None

    def delete_blob(self, digest):
        self.__logger.debug("Deleting blob: [{}]".format(digest))
        try:
            self._s3.Object(self._get_bucket_name(digest.hash),
                            self._construct_key(digest)).delete()
        except ClientError as e:
            if e.response['Error']['Code'] not in ['404', 'NoSuchKey']:
                raise

    def bulk_delete(self, digests):
        self.__logger.debug("Deleting blobs: [{}]".format(digests))
        buckets_to_digest_lists = {}
        failed_deletions = []
        for digest in digests:
            bucket = self._get_bucket_name(digest.hash)
            if bucket in buckets_to_digest_lists:
                buckets_to_digest_lists[bucket].append({'Key': self._construct_key(digest)})
            else:
                buckets_to_digest_lists[bucket] = [{'Key': self._construct_key(digest)}]
            if len(buckets_to_digest_lists[bucket]) >= self._page_size:
                # delete items for this bucket, hit page limit
                failed_deletions += self._multi_delete_blobs(bucket,
                                                             buckets_to_digest_lists.pop(bucket))
        # flush remaining items
        for bucket in buckets_to_digest_lists:
            failed_deletions += self._multi_delete_blobs(bucket, buckets_to_digest_lists[bucket])
        return failed_deletions

    def begin_write(self, _digest):
        # TODO use multipart API for large blobs?
        return io.BytesIO()

    def commit_write(self, digest, write_session):
        self.__logger.debug("Writing blob: [{}]".format(digest))
        write_session.seek(0)
        self._s3.Bucket(self._get_bucket_name(digest.hash)) \
                .upload_fileobj(write_session,
                                self._construct_key(digest))
        write_session.close()

    def is_cleanup_enabled(self):
        return True
