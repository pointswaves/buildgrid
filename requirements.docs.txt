Sphinx==2.3.1
sphinx-click==2.3.1
sphinx-rtd-theme==0.4.3 # For HTML search fix (upstream #672)
sphinxcontrib-apidoc==0.3.0
sphinxcontrib-napoleon==0.7
