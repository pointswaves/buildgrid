# Style
pep8==1.7.1
pylint==2.4.4

# Testing
pytest==5.3.2
pytest-cov==2.8.1
pytest-pep8==1.0.6
pytest-pylint==0.14.1
pytest-xdist==1.31.0

# Type checking
mypy==0.761
sqlalchemy-stubs==0.3
grpc-stubs==1.24.1
boto3-stubs==1.11.15.0